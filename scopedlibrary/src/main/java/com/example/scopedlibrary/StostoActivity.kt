package com.example.scopedlibrary

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.coroutines.launch
import java.io.File


const val REQUEST_PERMISSION_EXTERNAL_STORAGE = 2

class StostoActivity : AppCompatActivity() {
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_stosto)
		
		downloadImage()
	}
	
	private fun downloadImage() {
		val permission = listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
		if (checkPermissions(permission, REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
			downloadImageFromUrl("https://pixel.nymag.com/imgs/daily/vulture/2019/10/31/31-insider-lede-2.w700.h467.2x.jpg", object : RequestListener<File> {
				override fun onResourceReady(
					resource: File?,
					model: Any?,
					target: Target<File>?,
					dataSource: DataSource?,
					isFirstResource: Boolean
				): Boolean {
					saveImage(resource)
					return false
				}
				
				override fun onLoadFailed(
					e: GlideException?,
					model: Any?,
					target: Target<File>?,
					isFirstResource: Boolean
				): Boolean {
					lifecycleScope.launch {
						"failed".toast(this@StostoActivity)
					}
					return false
				}
			})
		}
	}
	
	override fun onRequestPermissionsResult(
		requestCode: Int,
		permissions: Array<out String>,
		grantResults: IntArray
	) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		if (requestCode == REQUEST_PERMISSION_EXTERNAL_STORAGE) {
			if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				downloadImage()
			}
		}
	}
	
	private fun saveImage(image: File?) {
		if (image == null) {
			lifecycleScope.launch {
				"image null".toast(this@StostoActivity)
			}
		}
		
		val storageDir = File(
			getExternalFilesDir(null),
			"acdef"
		)
		
		var success = true
		if (!storageDir.exists()) {
			success = storageDir.mkdirs()
		}
		
		if (success) {
			// Create the destination file
			// Create unique file name
			val dstFile = File(storageDir, "bwed.jpg")
			try {
				if (!dstFile.exists()) {
					// Copy from Glide cache to local
					image?.copyTo(dstFile)
					
				}
				lifecycleScope.launch {
					"success".toast(this@StostoActivity)
				}
			} catch (e: Exception) {
				e.printStackTrace()
				lifecycleScope.launch {
					"errro".toast(this@StostoActivity)
				}
			}
		}
	}
}

fun Any.toast(context: Context?) {
	context?.let {
		Toast.makeText(it, this.toString(), Toast.LENGTH_LONG).show()
	}
}


fun Context.downloadImageFromUrl(url: String, listener: RequestListener<File>) {
	Glide.with(this)
		.download(url)
		.listener(listener)
		.submit()
}


@SuppressLint("NewApi")
fun Activity.checkPermissions(permissions: List<String>, requestCode: Int): Boolean {
	if (permissions.isEmpty()) {
		return true
	}
	
	val permissionsNeeded = permissions.filter {
		ContextCompat.checkSelfPermission(this, it) != PermissionChecker.PERMISSION_GRANTED
	}
	
	if (permissionsNeeded.isEmpty()) {
		return true
	}
	
	requestPermissions(permissions.toTypedArray(), requestCode)
	return false
}
