package com.ghn.scopedstorage

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.scopedlibrary.StostoActivity

class StorageFragment : Fragment() {
	
	companion object {
		fun newInstance() = StorageFragment()
	}
	
	private lateinit var viewModel: StorageViewModel
	
	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.storage_fragment, container, false)
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		viewModel = ViewModelProviders.of(this).get(StorageViewModel::class.java)
		// TODO: Use the ViewModel
		
		val intent = Intent(requireContext(), StostoActivity::class.java)
		startActivity(intent)
		
	}
	
}

